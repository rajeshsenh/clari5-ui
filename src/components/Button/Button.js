import React from 'react';
import './Button.css';

const Button = () => (
  <button type="submit" className="btn">
    Click here
  </button>
);

export default Button;
