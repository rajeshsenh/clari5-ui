import resolve from '@rollup/plugin-node-resolve';
import babel from '@rollup/plugin-babel';
import postcss from 'rollup-plugin-postcss';
import { terser } from 'rollup-plugin-terser';
import path from 'path';

const production = !process.env.ROLLUP_WATCH;
const extensions = ['.mjs', '.js', '.jsx', '.json'];

export default {
  input: 'src/index.js',
  output: [
    {
      file: 'dist/index.es.js',
      format: 'es',
      name: 'clari5-ui',
      sourcemap: true,
    },
    // {
    //   file: 'dist/index.js',
    //   format: 'cjs',
    //   name: 'clari5-ui',
    // },
  ],
  plugins: [
    resolve(extensions),
    postcss({
      extensions: ['.css'],
      extract: path.resolve('dist/clari5-ui.css'),
    }),
    babel({
      babelHelpers: 'bundled',
      exclude: 'node_modules/**',
      presets: ['@babel/env', '@babel/preset-react'],
      extensions,
    }),
    production && terser(),
  ],
  external: ['react'],
};
