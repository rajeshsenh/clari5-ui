import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import '../node_modules/clari5-ui/dist/clari5-ui.css';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
