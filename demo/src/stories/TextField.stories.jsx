import React from 'react';

import { TextField } from 'clari5-ui';

export default {
  title: 'Demo/Input',
  component: TextField,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
};

const Template = (args) => <TextField {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  primary: true,
  label: 'TextField',
};
