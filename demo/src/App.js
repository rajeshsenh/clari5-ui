import CxButton from './Button/Button';

function App() {
  return (
    <div className="App">
      <CxButton />
    </div>
  );
}

export default App;
